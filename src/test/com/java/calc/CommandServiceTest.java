package test.com.java.calc;

import main.com.java.calc.CommandService;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.swing.*;

import static org.junit.jupiter.api.Assertions.*;

class CommandServiceTest {

    private static final CommandService cut = new CommandService();

    static Arguments[] concatNumbersTestArgs(){
        return new Arguments[]{
                Arguments.arguments("20", "2", new JButton("0")),
                Arguments.arguments("21", "2", new JButton("1")),
                Arguments.arguments("22", "2", new JButton("2")),
                Arguments.arguments("23", "2", new JButton("3")),
                Arguments.arguments("24", "2", new JButton("4")),
                Arguments.arguments("25", "2", new JButton("5")),
                Arguments.arguments("26", "2", new JButton("6")),
                Arguments.arguments("27", "2", new JButton("7")),
                Arguments.arguments("28", "2", new JButton("8")),
                Arguments.arguments("29", "2", new JButton("9")),
                Arguments.arguments("09", "0", new JButton("9")),
        };
    }

    @ParameterizedTest
    @MethodSource("concatNumbersTestArgs")
    void concatNumbersTest(String expected, String currentText, JButton b) {
        String actual = cut.concatNumbers(currentText, b);

        assertEquals(expected, actual);
    }

    static Arguments[] doOperationTestArgs(){
        return new Arguments[]{
                Arguments.arguments("", 25, new JButton("+")),
                Arguments.arguments("28", 3, new JButton("=")),
                Arguments.arguments("", 1, new JButton("C")),
                Arguments.arguments("", 10, new JButton("-")),
                Arguments.arguments("7", 3, new JButton("=")),
                Arguments.arguments("", 1, new JButton("C")),
                Arguments.arguments("", 12, new JButton("*")),
                Arguments.arguments("36", 3, new JButton("=")),
                Arguments.arguments("", 1, new JButton("C")),
                Arguments.arguments("", 50, new JButton("/")),
                Arguments.arguments("10", 5, new JButton("=")),
                Arguments.arguments("", 1, new JButton("C")),

        };
    }

    @ParameterizedTest
    @MethodSource("doOperationTestArgs")
    void doOperationTest(String expected, int argument, JButton b) {
        String actual = cut.doOperation(argument, b);

        assertEquals(expected, actual);
    }
}