package test.com.java.calc;

import main.com.java.calc.CommandService;
import main.com.java.calc.Panel;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import javax.swing.*;


import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class PanelTest {
    static Font font = new Font("SanSerif", Font.BOLD, 20);
    static JTextField output = new JTextField();/*Mockito.mock(JTextField.class);*/
    static JButton[] numbers = new JButton[10];
    static JButton backspace = new JButton("C");
    static JButton equal = new JButton("=");
    static JButton plus = new JButton("+");
    static JButton minus = new JButton("-");
    static JButton multiply = new JButton("*");
    static JButton divide = new JButton("/");
    static CommandService commandService = Mockito.mock(CommandService.class);
    static Panel cut = new Panel(font,output,numbers,backspace,equal, plus, minus, multiply,divide,commandService);

    static Arguments[] panelOperationsButtonsTestArgs(){
        return new Arguments[]{
                Arguments.arguments("1", backspace),
                Arguments.arguments("1", equal),
                Arguments.arguments("1", plus),
                Arguments.arguments("1", minus),
                Arguments.arguments("1", multiply),
                Arguments.arguments("1", divide),
        };
    }

    @ParameterizedTest
    @MethodSource("panelOperationsButtonsTestArgs")
    void panelOperationsButtonsTest(String arg, JButton button){
        output.setText(arg);
        button.doClick();
        Mockito.verify(commandService,Mockito.times(1)).doOperation(Integer.parseInt(arg), button);
    }

    static Arguments[] panelNumbersButtonsTestArgs(){
        return new Arguments[]{
                Arguments.arguments("10", "1", numbers[0]),
                Arguments.arguments("11", "1", numbers[1]),
                Arguments.arguments("12", "1", numbers[2]),
                Arguments.arguments("43", "4", numbers[3]),
                Arguments.arguments("3124", "312", numbers[4]),
                Arguments.arguments("1005", "100", numbers[5]),
                Arguments.arguments("996", "99", numbers[6]),
                Arguments.arguments("557", "55", numbers[7]),
                Arguments.arguments("128", "12", numbers[8]),
                Arguments.arguments("9879", "987", numbers[9])
        };
    }

    @ParameterizedTest
    @MethodSource("panelNumbersButtonsTestArgs")
    void panelNumbersButtonsTest(String expected, String textOnJTextField, JButton button){
        output.setText(textOnJTextField);
        Mockito.when(commandService.concatNumbers(textOnJTextField,button)).thenReturn(textOnJTextField + button.getText());
        button.doClick();
        assertEquals(expected,output.getText());
    }

}