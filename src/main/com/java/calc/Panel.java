package main.com.java.calc;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Panel extends JPanel {

    private Font font;
    private JTextField output;
    private JButton[] numbers;
    private JButton backspace;
    private JButton equal;
    private JButton plus;
    private JButton minus;
    private JButton multiply;
    private JButton divide;
    private CommandService commandService;
    private ActionListener numbersButtons;
    private ActionListener operationsButtons;

    public Panel(Font font, JTextField output, JButton[] numbers, JButton backspace, JButton equal, JButton plus,
                 JButton minus, JButton multiply, JButton divide, CommandService commandService) {
        this.font = font;
        this.output = output;
        this.numbers = numbers;
        this.backspace = backspace;
        this.equal = equal;
        this.plus = plus;
        this.minus = minus;
        this.multiply = multiply;
        this.divide = divide;
        this.commandService = commandService;

        setLayout(null);//дозволяє розміщувати елементи в будь якому порядку
        addButtons();
        addListeners();
    }

    private void addListeners(){
        numbersButtons = (ActionEvent e) -> {
            JButton b = (JButton) e.getSource();
            addNumberToTextField(b);
        };

        for(JButton b: numbers){
            b.addActionListener(numbersButtons);
        }

        operationsButtons = (ActionEvent e) -> {
            JButton b = (JButton) e.getSource();
            doOperation(b);
        };

        backspace.addActionListener(operationsButtons);
        equal.addActionListener(operationsButtons);
        plus.addActionListener(operationsButtons);
        minus.addActionListener(operationsButtons);
        multiply.addActionListener(operationsButtons);
        divide.addActionListener(operationsButtons);
    }

    private void addButtons(){
        backspace.setBounds(10, 250, 50, 50);
        backspace.setFont(font);
        add(backspace);

        equal.setBounds(130, 250, 50, 50);
        equal.setFont(font);
        add(equal);

        plus.setBounds(190, 70, 50, 50);
        plus.setFont(font);
        add(plus);

        minus.setBounds(190, 130, 50, 50);
        minus.setFont(font);
        add(minus);

        multiply.setBounds(190, 190, 50, 50);
        multiply.setFont(font);
        add(multiply);

        divide.setBounds(190, 250, 50, 50);
        divide.setFont(font);
        add(divide);

        numbers[0] = new JButton(String.valueOf(0));
        numbers[0].setBounds(70, 250, 50, 50);
        numbers[0].setFont(font);
        add(numbers[0]);
        for (int x = 0; x < 3; x++) {
            for(int y = 0; y < 3; y++){
                numbers[x * 3 + y + 1] = new JButton(String.valueOf(x * 3 + y + 1));
                numbers[x * 3 + y + 1].setBounds(y * (50 + 10) + 10, x * (50 + 10) + 70, 50, 50);
                numbers[x * 3 + y + 1].setFont(font);
                add(numbers[x * 3 + y + 1]);
            }
        }
        output.setBounds(10, 10, 230, 50);
        output.setFont(font);
        output.setEditable(false);//не можемо змінювати те, що там написано
        add(output);
    }

    private void addNumberToTextField(JButton b){
        output.setText(commandService.concatNumbers(output.getText(), b));
    }

    private void doOperation(JButton b){
        int firstArg = Integer.parseInt(output.getText());
        output.setText(commandService.doOperation(firstArg, b));
    }

}
