package main.com.java.calc;

import javax.swing.*;

public class CommandService {
    private int firstArg = 0;
    private int secondArg = 0;
    private String operation = "+";

    public String concatNumbers(String currentText, JButton b){
        return currentText + b.getText();
    }

    public String doOperation(int argument, JButton b) {
        int result = 0;
        String text = "";
        switch(b.getText()){
            case "=":
                secondArg = argument;
                result = doOperation(firstArg, secondArg, operation);
                text = String.valueOf(result);
                break;
            case "C":
                firstArg = 0;
                text = "";
                break;
            case "+":
                firstArg = argument;
                text = "";
                operation = "+";
                break;
            case "-":
                firstArg = argument;
                text = "";
                operation = "-";
                break;
            case "/":
                firstArg = argument;
                text = "";
                operation = "/";
                break;
            case "*":
                firstArg = argument;
                text = "";
                operation = "*";
                break;
        }
        return text;
    }

    private int doOperation(int a, int b, String operation){
        int result = 0;
        switch (operation){
            case "+":
                result = a + b;
                break;
            case "-":
                result = a - b;
                break;
            case "/":
                result = a / b;
                break;
            case "*":
                result = a * b;
                break;
            default:
                result = 0;
        }
        return result;
    }
}
