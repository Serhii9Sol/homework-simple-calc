package main.com.java.calc;

import javax.swing.*;
import java.awt.*;

public class Start {

    private JFrame window;

    public Start(){
        Font font = new Font("SanSerif", Font.BOLD, 20);
        JTextField output = new JTextField();
        JButton[] numbers = new JButton[10];
        JButton backspace = new JButton("C");
        JButton equal = new JButton("=");
        JButton plus = new JButton("+");
        JButton minus = new JButton("-");
        JButton multiply = new JButton("*");
        JButton divide = new JButton("/");
        CommandService commandService = new CommandService();
        Panel panel = new Panel(font,output,numbers,backspace,equal, plus, minus, multiply,divide,commandService);

        window = new JFrame("Calculator");
        window.setSize(260, 350);
        window.add(panel);
        window.setLocationRelativeTo(null);
        window.setResizable(false);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);
    }
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Start();
            }
        });
    }
}
